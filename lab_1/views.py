from django.shortcuts import render
import datetime

date = {'date' : datetime.datetime.now()}

def index(request):
    return render(request, 'index.html', date)

def index2(request):
    return render(request, 'index2.html', date)

def index3(request):
    return render(request, 'index3.html', date)

def index4(request):
    return render(request, 'index4.html', date)
