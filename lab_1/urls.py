from django.urls import re_path
from .views import index, index2, index3, index4
#url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path(r'^aboutme/', index2, name='index2'),
    re_path(r'^resume/', index3, name='index3'),
    re_path(r'^contacts/', index4, name='index4'),
]
