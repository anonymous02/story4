from django.conf.urls import include
from django.urls import re_path
from django.contrib import admin
from lab_1.views import index as index
from lab_1.views import index2 as index2
from lab_1.views import index3 as index3
from lab_1.views import index4 as index4


urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^lab-1/', include('lab_1.urls')),
    re_path(r'^$', index, name='index'),
    re_path(r'^aboutme/', index2, name='index2'),
    re_path(r'^resume/', index3, name='index3'),
    re_path(r'^contacts/', index4, name='index4'),

]
